package pos_java_jdbc.pos_java_jdbc;

import java.util.List;

import org.junit.Test;

import dao.UserposDAO;
import model.Telefone;
import model.UserposJava;
import vo.BeanUserFone;

public class TesteBancoJdbc {

	@Test
	public void initBanco() {

		UserposDAO userposDao = new UserposDAO();
		UserposJava userposJava = new UserposJava();

		userposJava.setEmail("fulano@gmail.com");
		userposJava.setNome("Fulano");
		
		userposDao.salvar(userposJava);
	}

	@Test
	public void initListar() {
		UserposDAO userposDao = new UserposDAO();

		List<UserposJava> list = userposDao.listar();

		for (UserposJava user : list) {

			System.out.println(user);
			System.out.println("---------------------");
		}
	}

	@Test
	public void initbuscar() {

		UserposDAO userposDao = new UserposDAO();

		UserposJava user = userposDao.buscar(2L);
		
		System.out.println("---------------------");
		System.out.println(user);
	}
	
	@Test
	public void initatualizar() {
		
		
		UserposDAO posJava = new UserposDAO();
		UserposJava user = posJava.buscar(1L);

		user.setNome("Suru");
		user.setEmail("surucucu@gmail");
		
		posJava.atualizar(user);
		
		System.out.println("---------------------");
		System.out.println(user);
	}
	
	@Test
	public void initdeletar() {
		try {
		UserposDAO dao = new UserposDAO();
		dao.deletar(2L);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void initInsertTelefone() {
		try {
		UserposDAO dao = new UserposDAO();
		Telefone tel = new Telefone();
		
		tel.setNumero(" 51 9654-7842");
		tel.setTipo("celular");
		tel.setUsuario(8L);
		
		dao.salvarTelefone(tel);
		
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void initCarregaFoneUser() {
		try {
		UserposDAO dao = new UserposDAO();

		List<BeanUserFone> bean = dao.listaUserFone(8L);
		
		for( BeanUserFone b : bean) {
			System.out.println("----------------");
			System.out.println(b);
		}
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void initDeleteUserFone() {
		try {
		UserposDAO dao = new UserposDAO();
		dao.deleteFonesByUser(8L);
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
}
