package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conexaojdbc.SingleConnection;
import model.Telefone;
import model.UserposJava;
import vo.BeanUserFone;

public class UserposDAO {

	private Connection connection;
	
	public UserposDAO() {
		connection = SingleConnection.getConnection();
	}
	
	public void salvar(UserposJava user) {
		
		try {
			String sql = "insert into userposjava (nome, email) values (?,?)";
			PreparedStatement insert = connection.prepareStatement(sql);
			
			insert.setString(1, user.getNome());
			insert.setString(2, user.getEmail());
			insert.execute();
			
			connection.commit();
			
		} catch (SQLException e) {
			
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
	}
	
	public void salvarTelefone(Telefone tel) {
		
		try {
			String sql = "insert into telefoneuser (numero, tipo, usuariopessoa) values (?,?,?)";
			PreparedStatement insert = connection.prepareStatement(sql);
			
			insert.setString(1, tel.getNumero());
			insert.setString(2, tel.getTipo());
			insert.setLong(3, tel.getUsuario());
			insert.execute();
			
			connection.commit();
			
			
		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public List<BeanUserFone> listaUserFone(Long idUser){
		
		List<BeanUserFone> bean = new ArrayList<BeanUserFone>();
		String sql = " select nome, numero, email from telefoneuser as fone ";
			   sql += " inner join userposjava as userp ";
			   sql += " on fone.usuariopessoa = userp.id ";
			   sql += " where userp.id = ? ";
			   
			try {
				PreparedStatement st = connection.prepareStatement(sql);
				st.setLong(1, idUser);
				
				ResultSet result =  st.executeQuery();
				
				while(result.next()) {
					
					BeanUserFone user  = new BeanUserFone();
					
					user.setNome(result.getString("nome"));
					user.setEmail(result.getString("email"));
					user.setNumero(result.getString("numero"));
					
					bean.add(user);
					
				}
				
				connection.commit();
				
			} catch (SQLException e) {
				try {
					connection.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
			   
		
		return bean;
	}
	
	public List<UserposJava> listar(){
		
		List<UserposJava> list = new ArrayList<UserposJava>();
		
		String sql = "select * from userposjava";
		
		try {
			PreparedStatement st = connection.prepareStatement(sql);
			ResultSet resultado = st.executeQuery();
			
			while(resultado.next()) {
				
				UserposJava user = new UserposJava();
				
				user.setId(resultado.getLong("id"));
				user.setNome(resultado.getString("nome"));
				user.setEmail(resultado.getString("email"));
				
				list.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;	
	}
	
	public UserposJava buscar(Long id){
		
		UserposJava usuario = new UserposJava();
		
		String sql = "select * from userposjava where id = "+ id ;
		
		try {
			PreparedStatement st = connection.prepareStatement(sql);
			ResultSet resultado = st.executeQuery();
			
			while(resultado.next()) {
				
				usuario.setId(resultado.getLong("id"));
				usuario.setNome(resultado.getString("nome"));
				usuario.setEmail(resultado.getString("email"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return usuario;	
	}
	
	
	public void atualizar(UserposJava user) {
		
		String sql = "update userposjava set nome = ?, email = ? where id = ? ";
		
		try {
			PreparedStatement st = connection.prepareStatement(sql);
			st.setString(1, user.getNome());
			st.setString(2, user.getEmail());
			st.setLong(3, user.getId());
			st.execute();
			
			connection.commit();
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
	}
	
	public void deletar(Long id) {
		
		String sql = "delete from userposjava where id = ? ";
		
		try {
			PreparedStatement st = connection.prepareStatement(sql);
			st.setLong(1, id);
			st.execute();
			
			connection.commit();
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}
	
	public void deleteFonesByUser(Long id) {
		
		String sqlFone = "delete from telefoneuser where usuariopessoa = " + id;
		String sqlUser = "delete from userposjava where id = " + id;
		
		try {
			PreparedStatement st = connection.prepareStatement(sqlFone);
			st.executeUpdate();
			connection.commit();
			
			st = connection.prepareStatement(sqlUser);
			st.executeUpdate();
			connection.commit();
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}
	
			
}
